package main

import (
	"fmt"
	"log"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

func producer(id int, numJobs int, jobs <-chan Record, results chan string) {


	fmt.Printf("Producer %d Starting\n", id)
	deliveryResult := make(chan kafka.Event, 1000)

	producer, err := kafka.NewProducer(&kafka.ConfigMap{
		"bootstrap.servers": "localhost:9092",
	})
	if err != nil {
		log.Fatal(err)
	}

	for i := 0; i < numJobs; i++ {
		record := (<-jobs).(NormalRecord)

		producer.Produce(&kafka.Message{
			TopicPartition: kafka.TopicPartition{Topic: &record.topic, Partition: kafka.PartitionAny},
			Value:          []byte(record.ToJson()),
		}, deliveryResult)

		e := <-deliveryResult
		m := e.(*kafka.Message)

		if m.TopicPartition.Error != nil {
			result := fmt.Sprintf("Delivery failed: %v\n", m.TopicPartition.Error)
			results <- result
		} else {
			result := fmt.Sprintf("Delivered message to topic %s [%d] at offset %v\n",
				*m.TopicPartition.Topic, m.TopicPartition.Partition, m.TopicPartition.Offset)
			results <- result
		}
	}
}
