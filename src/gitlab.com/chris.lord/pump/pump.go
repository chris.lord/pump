package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/manveru/faker"
)

// Record : Define an interface for a generic record
type Record interface {
	ToJSON() []byte
}

// NormalRecord : Define a standard test record
type NormalRecord struct {
	topic       string
	Name        string
	Time        time.Time
	Address     string
	PhoneNumber string
	Blob        []string
}

// ToJSON : Marshal NormalRecord to JSON
func (record NormalRecord) ToJSON() []byte {
	jsonRecord, err := json.Marshal(&record)
	if err != nil {
		log.Fatal(err)
	}
	return jsonRecord
}

// ElasticSearchRecord : Define a one off record for iheartmedia elasticsearch testing
type ElasticSearchRecord struct {
	OpportunityID uuid.UUID
	ProposalID    uuid.UUID
	Market         []uuid.UUID
	Station        []uuid.UUID
	Status         string
}

// ToJSON : Marshal ElasticSearchRecord to JSON
func (record ElasticSearchRecord) ToJSON() []byte {
	jsonRecord, err := json.Marshal(&record)
	if err != nil {
		log.Fatal(err)
	}
	return jsonRecord
}

func generateElasticSearchData(messages int, jobs chan<- Record) {
	/*
		Generate random data using the elasticsearchRecord struct for testing iheartmedia's issue

		Input:
		messages Integer - Number of messages to generate
		jobs chan<- elasticsearchRecord - the channel to push generated data onto
	*/

	reasons := []string{"NEW", "OPEN", "COMPLETE"}

	for i := 0; i < messages; i++ {
		record := ElasticSearchRecord{
			OpportunityID: uuid.New(),
			ProposalID:    uuid.New(),
			Market:         []uuid.UUID{uuid.New(), uuid.New()},
			Station:        []uuid.UUID{uuid.New(), uuid.New()},
			Status:         fmt.Sprint(reasons[rand.Intn(len(reasons))]),
		}
		jobs <- record
	}
}

func generateData(messages int, jobs chan<- Record, timestamp int64) {
	/*
		Generate random data using the record struct and push it into a jobs channel

		Input:
		messages Integer - Number of messages to generate
		jobs chan<- NormalRecord - The channel to push generated data onto

		Output:
		nil
	*/
	fake, err := faker.New("en")
	if err != nil {
		log.Fatal(err)
	}

	for i := 0; i < messages; i++ {
		fmt.Printf("Job queue contains %d of %d total jobs\n", len(jobs), cap(jobs))
		num := rand.Intn(15)
		i64 := int64(i)
		timestamp := time.Unix(timestamp+i64, 0)
		fmt.Println(timestamp)

		record := NormalRecord{
			topic:       "test",
			Name:        fake.Name(),
			Time:        timestamp,
			Address:     fmt.Sprintf("%s %s %s %s", fake.StreetAddress(), fake.City(), fake.State(), fake.PostCode()),
			PhoneNumber: fake.PhoneNumber(),
			Blob:        fake.Paragraphs(num, true),
		}
		jobs <- record

	}
	close(jobs)
}

func main() {

	fmt.Printf("Starting the pump\n")

	/*
		Define the huge number of flags required to handle the large amount of stuff this program does.
	*/

	threads := flag.Int("threads", 10, "Set number of threads. Defaults to 10")
	messages := flag.Int("messages", 100000, "Number of messages to send. Defaults to 100000")
	datastore := flag.String("datastore", "cassandra", "The datastore application to pump data into. Defaults to cassandra")
	hosts := flag.String("hosts", "localhost", "List of comma separated hosts to connect to. Defaults to localhost")
	port := flag.Int("port", 0, "Port to connect to. Defaults to 9092 for Kafka and 9042 for Cassandra")
	keyspace := flag.String("keyspace", "faker", "The name fo the keyspace to be created")
	replicationStrategy := flag.String("replication-strategy", "SimpleStrategy", "The replication strategy to use when creating the keyspace")
	replicationFactor := flag.Int("replication-factor", 1, "Replication factor to use. Defaults to 1")
	datacentres := flag.String("datacentres", "", "The names of the data centres to be used for the NetworkTopology replication strategy")
	authentication := flag.String("authentication", "", "The authentication protocol to use. Defaults to nil")
	username := flag.String("username", "", "The username to use")
	password := flag.String("password", "", "The password to use")
	url := flag.String("url", "", "The url to use to insert data into elasticsearch")
	timeout := flag.Int("timeout", 10, "The time out in seconds for elasticsearch API. Defaults to 10.")
	lucene := flag.Bool("lucene", false, "Create a lucene index. Defaults to false.")
	timestamp := flag.Int64("timestamp", 0, "The timestamp to start generating dates from. Defaults to 0.")

	flag.Parse()

	results := make(chan string, 1000)
	jobs := make(chan Record, 1000)

	fmt.Println("Starting Data Generation")

	switch strings.ToLower(*datastore) {
	case "iheartmedia":
		go generateElasticSearchData(*messages, jobs)
	default:
		go generateData(*messages, jobs, *timestamp)
	}

	numJobs := *messages / *threads

	switch strings.ToLower(*datastore) {
	case "cassandra":
		if *port == 0 {
			*port = 9042
		}
		cluster := prepareCassandra(*hosts, *port, *authentication, *username, *password, *keyspace, *replicationStrategy, *replicationFactor, *datacentres, *lucene)
		fmt.Println("Creating Cassandra Workers")
		for i := 0; i < *threads; i++ {
			go cassandraWorker(i, numJobs, jobs, results, cluster)
		}
	case "elasticsearch":
		if *port == 0 {
			*port = 9200
		}
		if *url == "" {
			log.Fatal("url can't be blank")
		}
		fmt.Println("Creating elasticsearch workers")
		for i := 0; i < *threads; i++ {
			go elasticsearchWorker(i, numJobs, *url, *username, *password, jobs, results, *timeout)
		}
	case "iheartmedia":
		if *port == 0 {
			*port = 9200
		}
		if *url == "" {
			log.Fatal("url can't be blank")
		}
		fmt.Println("Creating elasticsearch workers")
		for i := 0; i < *threads; i++ {
			go elasticsearchWorker(i, numJobs, *url, *username, *password, jobs, results, *timeout)
		}
	}

	for i := 0; i < *messages; i++ {
		fmt.Printf("Results queue contains %d of %d total jobs\n", len(results), cap(results))
		fmt.Println(<-results)
	}

	os.Exit(0)

}
