package main

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/gocql/gocql"
)

func cassandraWorker(id int, numJobs int, jobs chan Record, results chan string, cluster *gocql.ClusterConfig) {

	session, err := cluster.CreateSession()
	if err != nil {
		log.Output(2, err.Error())
	}
	defer session.Close()

	for i := 0; i < numJobs; i++ {
		record := (<-jobs).(NormalRecord)

		date := time.Date(record.Time.Year(), record.Time.Month(), record.Time.Day(), 0, 0, 0, 0, time.UTC)

		if err := session.Query(`INSERT INTO faker.faked (date, id, time, name, address, phone_number, blob) VALUES (?, uuid(), ?, ?, ?, ?, ?)`, date, record.Time, record.Name, record.Address, record.PhoneNumber, strings.Join(record.Blob, " ")).Exec(); err != nil {
			jobs <- record
			fmt.Printf("%s", date)
			log.Fatal(err)
		} else {
			results <- fmt.Sprintf("Inserted data for user %s", record.Name)
		}
	}
}

func prepareCassandra(hosts string, port int, authentication string, username string, password string, keyspace string, replicationStrategy string, replicationFactor int, datacentres string, lucene bool) *gocql.ClusterConfig {
	hostList := strings.Split(hosts, ",")
	datacentreList := strings.Split(datacentres, ",")
	cluster := gocql.NewCluster()
	cluster.Hosts = hostList
	cluster.Port = port
	cluster.ConnectTimeout = 10000 * time.Millisecond
	cluster.Timeout = 10000 * time.Millisecond
	cluster.RetryPolicy = &gocql.SimpleRetryPolicy{
		NumRetries: 4,
	}
	cluster.ProtoVersion = 4
	cluster.PoolConfig.HostSelectionPolicy = gocql.DCAwareRoundRobinPolicy(datacentreList[0])
	cluster.Consistency = gocql.LocalQuorum

	if authentication == "PasswordAuthenticator" {
		cluster.Authenticator = &gocql.PasswordAuthenticator{
			Username: username,
			Password: password,
		}
	}

	session, err := cluster.CreateSession()
	if err != nil {
		log.Output(2, err.Error())
	}
	defer session.Close()

	var keyspaceQuery string

	switch replicationStrategy {
	case "NetworkTopologyStrategy":
		keyspaceQuery = fmt.Sprintf("CREATE KEYSPACE IF NOT EXISTS %s WITH REPLICATION = { 'class': '%s'", keyspace, replicationStrategy)
		for _, datacentre := range datacentreList {
			tempString := fmt.Sprintf(", '%s': %d", datacentre, replicationFactor)
			keyspaceQuery += tempString
		}
		keyspaceQuery += "};"
	default:
		keyspaceQuery = fmt.Sprintf("CREATE KEYSPACE IF NOT EXISTS %s WITH REPLICATION = { 'class': 'SimpleStrategy', 'replication_factor': %d};", keyspace, replicationFactor)
	}

	if err := session.Query(keyspaceQuery).Exec(); err != nil {
		fmt.Println(keyspaceQuery)
		log.Output(2, err.Error())
	}
	if err := session.Query(fmt.Sprintf(`CREATE TABLE IF NOT EXISTS %s.faked (
		id UUID,
		date date,
		time timestamp,
		name text,
		address text,
		phone_number text,
		blob text,
		PRIMARY KEY (date, id)
	);`, keyspace)).Exec(); err != nil {
		log.Output(2, err.Error())
	}
	if lucene == true {
		if err := session.Query(fmt.Sprintf(`CREATE CUSTOM INDEX lucene_faked ON %s.faked ()
		USING 'com.stratio.cassandra.lucene.Index'
		WITH OPTIONS = {
		   'refresh_seconds': '1',
		   'schema': '{
			  fields: {
				 id: {type: "uuid"},
				 date: {type: "date", pattern: "yyyy/MM/dd"},
				 time: {type: "date", pattern: "HH:mm:ss.SSS Z"},
				 name: {type: "string"},
				 address: {type: "string"},
				 phone_number: {type: "string"},
				 blob: {type: "string"}
			  }
		   }'
		};`, keyspace)).Exec(); err != nil {
			log.Output(2, err.Error())
		}
	}

	return cluster

}
