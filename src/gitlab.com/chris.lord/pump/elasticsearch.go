package main

import (
	"encoding/json"
	"fmt"
	"log"

	"bytes"

	retryablehttp "github.com/hashicorp/go-retryablehttp"
)

func elasticsearchWorker(id int, numJobs int, url string, username string, password string, jobs <-chan Record, results chan string, timeout int) {

	fmt.Printf("elasticsearch worker %d Starting\n", id)

	for i := 0; i < numJobs; i++ {
		record := <-jobs

		jsonRecord, err := json.Marshal(record)
		if err != nil {
			log.Fatal(err)
		}

		response, err := retryablehttp.Post(fmt.Sprintf("%s/_doc", url), "application/json", bytes.NewBuffer(bytes.ToLower(jsonRecord)))
		if err != nil {
			log.Fatal(err)
		}
		if response.StatusCode != 201 {
			fmt.Println(response.Status)
		} else {
			results <- response.Status
		}
		response.Body.Close()

	}
}
